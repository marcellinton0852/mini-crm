<?php

namespace Tests\Feature\Http\Controllers;
use App\Models\User;
use App\Models\Companies;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;

class ApiControllerTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function api_user_login()
    {
        $user = User::factory(1)->create()->first();

        $this->json('POST','api/login', ['email' => $user->email, 'password' => 'password'])
        ->assertStatus(200)
        ->assertJsonStructure([
            "user" => [
                'id',
                'name',
                'email',
                'email_verified_at',
                'created_at',
                'updated_at',
            ],
             "token" => [
                 'token',
             ]
        ]);

     $this->assertAuthenticated();
    }

    /** @test */
    public function api_user_view_company_list()
    {
        $user = User::factory(1)->create()->first();

        $this->json('POST','api/login', ['email' => $user->email, 'password' => 'password'])
        ->assertStatus(200)
        ->assertJsonStructure([
            "user" => [
                'id',
                'name',
                'email',
                'email_verified_at',
                'created_at',
                'updated_at',
            ],
             "token" => [
                 'token',
             ]
        ]);
        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        return $this->get(route('company.show', $company))->assertStatus(200);
    }

    /** @test */
    public function api_user_logout()
    {
        $user = User::factory(1)->create()->first();
        $this->actingAs($user);
        return $this->post(route('api.logout', $user))->assertStatus(200);
    }
}
