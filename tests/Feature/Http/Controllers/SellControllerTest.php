<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Companies;
use App\Models\Employee;
use App\Models\Sell;
use App\Models\User;
use App\Models\Item;
use App\Models\SellSummary;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SellControllerTest extends TestCase
{
    use RefreshDatabase;
    /**  @test */
    public function user_view_sell()
    {
        $response = $this->get('dashboard/sell');

        $response->assertRedirect('/login');
    }

    /**  @test */
    public function user_create_sell_and_sellsummary()
    {
        $user = User::factory(1)->create()->first();
        $item = Item::create([
            'name'=>'test',
            'price'=> 10
        ]);
        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => '1',
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id,
        ]);

        $response = $this->actingAs($user)->post(route('sell.store'), [
            'date'=>'2021-11-12',
            'item_id'=>$item->id,
            'price'=> 20,
            'discount'=> 10,
            'employee_id'=> $employee->id,
        ]);

        $response = $this->actingAs($user)->post(route('sellsummary.store'), [
            'date'=>'2021-11-12',
            'price_total'=> 20,
            'discount_total'=> 10,
            'total'=> 10,
            'employee_id'=> $employee->id,
        ]);
        return $response->assertStatus(200);
    }

    /** @test */
    public function user_update_sell_and_update_or_create_sell_summary()
    {
        $user = User::factory(1)->create()->first();
        $item = Item::create([
            'name'=>'test',
            'price'=> 10
        ]);
        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => '1',
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id,
        ]);
        $sell = Sell::create([
            'date'=>'2021-11-12',
            'item_id'=>$item->id,
            'price'=> 20,
            'discount'=> 10,
            'employee_id'=> $employee->id,
        ]);
        $response = $this->actingAs($user)->put(route('sell.update', $sell), [
            'date'=>'2021-11-12',
            'item_id'=>$item->id,
            'price'=> 10,
            'discount'=> 5,
            'employee_id'=> $employee->id,
        ]);
        return $response->assertStatus(302);
    }

    /** @test */
    public function user_delete_sell()
    {
        $user = User::factory(1)->create()->first();
        $item = Item::create([
            'name'=>'test',
            'price'=> 10
        ]);
        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => '1',
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id,
        ]);
        $sell = Sell::create([
            'date'=>'2021-11-12',
            'item_id'=>$item->id,
            'price'=> 20,
            'discount'=> 10,
            'employee_id'=> $employee->id,
        ]);
        $response = $this->actingAs($user)->delete(route('sell.destroy', $sell));
        return $response->assertStatus(302);
    }
    /** @test */
    public function user_can_update_summary()
    {
        $user = User::factory(1)->create()->first();
        $item = Item::create([
            'name'=>'test',
            'price'=> 10
        ]);
        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => '1',
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id,
        ]);

        $sell = Sell::create([
            'date'=>'2021-11-12',
            'item_id'=>$item->id,
            'price'=> 20,
            'discount'=> 10,
            'employee_id'=> $employee->id,
        ]);

        $summary = SellSummary::create([
            'date'=>'2021-11-12',
            'price_total'=> $sell->price,
            'discount_total'=> $sell->discount,
            'total'=> $sell->price - $sell->discount,
            'employee_id'=> $employee->id,
        ]);

        $response = $this->actingAs($user)->put(route('sell.update', $sell),[
            'date'=>'2021-11-12',
            'item_id'=>$item->id,
            'price'=> 30,
            'discount'=> 20,
            'employee_id'=> $employee->id,
        ]);
        $response = $this->actingAs($user)->post(route('sellsummary.store', $summary), [
            'date'=>'2021-11-12',
            'price_total'=> $sell->price,
            'discount_total'=> $sell->discount,
            'total'=> $sell->price - $sell->discount,
            'employee_id'=> $employee->id,
        ]);
        return $response->assertStatus(200);}
}
