<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use App\Models\Companies;
use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CompanyEmployeeControllerTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function user_view_employee_()
    {
        $response = $this->get('dashboard/employee');

        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_create_company_employee()
    {
        $user = User::factory(1)->create()->first();

        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        $response = $this->actingAs($user)->post(route('employee.store'), [
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => $company->id,
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123'
        ]);

        return $response->assertStatus(302);
    }

    /** @test */
    public function user_view_company_employee_list()
    {
        $user = User::factory(1)->create()->first();

        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => $company->id,
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        return $this->get(route('employee.show', $employee))->assertStatus(302);
    }

    /** @test */
    public function user_update_company_employee()
    {
        $user = User::factory(1)->create()->first();

        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => $company->id,
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        $employee = $this->actingAs($user)->put(route('employee.update', $employee), [
            'first_name' => 'tester1',
            'last_name' => '1ertset',
            'email' => 'tester1@gmail.com',
            'phone' => '1231',
            'password' => '1231',
        ]);
        return $employee->assertStatus(302);
    }

    /** @test */
    public function user_delete_company_employee()
    {
        $user = User::factory(1)->create()->first();

        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => $company->id,
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        $field = $this->actingAs($user)->delete(route('employee.destroy', $employee));

        return $field->assertStatus(302);
    }
}
