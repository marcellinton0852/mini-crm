<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Companies;
use App\Models\user;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CompaniesControllerTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function user_view_company_dashboard()
    {
        $this->get('/dashboard/company')->assertRedirect('/login');
    }

    /** @test */
    public function user_create_company()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->actingAs($user)->post(route('company.store'),[
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        return $response->assertStatus(302);
    }

    /** @test */
    public function user_view_company_list()
    {
        $user = User::factory(1)->create()->first();

        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        return $this->get(route('company.show', $company))->assertStatus(302);
    }

    /** @test */
    public function user_update_company()
    {
        $user = User::factory(1)->create()->first();
        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        $company = $this->actingAs($user)->put(route('company.update', $company), [
            'name' => 'testing2',
            'email' => 't2@s',
            'website' => 'test2',
        ]);
        return $company->assertStatus(302);
    }

    /** @test */
    public function user_destroy_company()
    {
        $user = User::factory(1)->create()->first();
        $company = Companies::create([
            'name' => 'testing',
            'email' => 't@s',
            'website' => 'test',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        $field = $this->actingAs($user)->delete(route('company.destroy', $company));

        return $field->assertStatus(302);
    }
}
