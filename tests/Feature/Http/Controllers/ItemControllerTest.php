<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Item;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ItemControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_view_item()
    {
        $this->get('dashboard/item')->assertRedirect('/login');
    }

    /** @test */
    public function user_create_item()
    {
        $user = User::factory(1)->create()->first();
        $this->actingAs($user);

        $response = $this->actingAs($user)->post(route('item.store'),[
            'name' => 'test',
            'price' => '10',
        ]);
        return $response->assertStatus(302);
    }

    /** @test */
    public function user_view_item_list()
    {
        $user = User::factory(1)->create()->first();
        $this->actingAs($user);

        $item = Item::create([
            'name' => 'test',
            'price' => '10',
        ]);
        return $this->get(route('item.show', $item))->assertStatus(200);
    }

    /** @test */
    public function user_update_item()
    {
        $user = User::factory(1)->create()->first();

        $item = Item::create([
            'name' => 'test',
            'price' => '10',
        ]);
        $response = $this->actingAs($user)->put(route('item.update', $item),[
            'name' => 'test1',
            'price' => '101',
        ]);
        return $response->assertStatus(302);
    }

    /** @test */
    public function user_delete_item()
    {
        $user = User::factory(1)->create()->first();

        $item = Item::create([
            'name' => 'test',
            'price' => '10',
        ]);
        $response = $this->actingAs($user)->delete(route('item.destroy', $item));
        return $response->assertStatus(302);
    }
}

