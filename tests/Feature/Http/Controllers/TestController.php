<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Companies;
use App\Models\Employee;
use App\Models\User;
use App\Models\Item;
use App\Models\Sell;
use App\Models\SellSummary;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TestController extends TestCase
{
    use RefreshDatabase;

}
