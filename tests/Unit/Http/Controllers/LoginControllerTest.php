<?php

namespace Tests\Unit\Http\Controllers;

use App\Models\User;
use App\Models\Companies;
use App\Models\Employee;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /** @test */
    public function login()
    {
        $user = User::factory(1)->create()->first();

        $login = Auth::attempt([
            'email' => $user->email,
            'password' => 'password']);

        return $this->assertTrue($login);
    }

    /** @test */
    public function employee_login()
    {
        $user = User::factory(1)->create()->first();
        $company = Companies::Create([
            'name' => $this->faker->company(),
            'email' => $this->faker->companyEmail(),
            'website' => $this->faker->domainName(),
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        $this->get("/employee/login/$company->id")->assertStatus(200);
    }

    /** @test */
    public function logout()
    {
        $user = User::factory(1)->create()->first();
        $this->actingAs($user);
        return $this->post(route('logout', $user))->assertStatus(302);
    }

    /** @test */
    public function employee_logout()
    {
        $user = User::factory(1)->create()->first();
        $company = Companies::Create([
            'name' => $this->faker->company(),
            'email' => $this->faker->companyEmail(),
            'website' => $this->faker->domainName(),
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        $employee = Employee::create([
            'first_name' => 'tester',
            'last_name' => 'ertset',
            'companies_id' => $company->id,
            'email' => 'tester@gmail.com',
            'phone' => '123',
            'password' => '123',
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        $this->actingAs($employee);
        return $this->post(route('employee.logout', $user))->assertStatus(302);
    }
}
