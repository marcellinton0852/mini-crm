<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Companies;

class RouteTest extends TestCase
{
    /** @test */
    public function redirect_default_page_to_login()
    {
        $this->get('/')->assertSee('login');
    }

    /** @test */
    public function redirect_to_dashboard()
    {
        $this->get('/dashboard')->assertSee('login');
    }

    /** @test */
    public function redirect_to_dashboard_company()
    {
        $this->get('/dashboard/company')->assertSee('login');
    }

    /** @test */
    public function redirect_to_dashboard_employee()
    {
        $this->get('/dashboard/employee')->assertSee('login');
    }

    /** @test */
    public function redirect_to_dashboard_company_employee()
    {
        $this->get('/dashboard/company/{company}/employee')->assertSee('login');
    }

    /** @test */
    public function register_page_is_not_available()
    {
        $this->get('/register')->assertStatus(404);
    }

    /** @test */
    public function change_website_language_to_indonesia()
    {
        $this->get('/lang/id')->assertSessionHas('locale','id');
    }

    /** @test */
    public function change_website_language_to_english()
    {
        $this->get('/lang/en')->assertSessionHas('locale','en');
    }

    /** @test */
    public function prefix_employee_dashboard()
    {
        $this->get('/employee/dashboard')->assertStatus(302);
    }

    /** @test */
    public function prefix_employee_list()
    {
        $this->get('/employee/list')->assertStatus(302);
    }
}
