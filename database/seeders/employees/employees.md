employees
=========

| companies_id | first_name | last_name |        email        |  phone   | password | created_by_id | updated_by_id |
|--------------|------------|-----------|---------------------|----------|----------|---------------|---------------|
| 1            | qwrqw      | asxzv     | acomasde@gmail.com  | 14836    | qwe      | 1             | 1             |
| 2            | qwrsf      | agase     | acome@gmail.com     | 26437    | qwer     | 1             | 1             |
| 3            | qwrtzx     | rsdxb     | acome@gmail.com     | 7456859  | qwr      | 1             | 1             |
| 4            | ewh        | rdfnx     | acome@gmail.com     | 32526    | qr       | 1             | 1             |
| 5            | wywr       | fgdry     | no@no.com           | 464743   | qwr      | 1             | 1             |
| 1            | rwusfsh    | gfjfgi    | outllok@outlook.com | 427247   | qwr      | 1             | 1             |
| 2            | rjeue      | rejuc     | asd@gmail.com       | 8546     | asf      | 1             | 1             |
| 1            | reji       | giz       | jean@gmail.com      | 1515325  | zxc      | 1             | 1             |
| 2            | owen       | gigaz     | lok@outl.com        | 262362   | af       | 1             | 1             |
| 3            | rock       | tomaz     | scream@look.com     | 32156    | sdh      | 1             | 1             |
| 1            | xina       | toms      | lear@gma.com        | 2623151  | wyew     | 1             | 1             |
| 1            | jhon       | jerry     | cnt@gm.com          | 135327   | wey      | 1             | 1             |
| 2            | lary       | diluc     | cnt@gm.com          | 43745848 | wey      | 1             | 1             |
| 4            | lock       | venti     | cnt@gm.com          | 436437   | afs      | 1             | 1             |
| 5            | giar       | jean      | cnt@gm.com          | 26136    | yesy     | 1             | 1             |
| 6            | gant       | mona      | cnt@gm.com          | 3764     | seysd    | 1             | 1             |
(16 rows)

