<?php

namespace Database\Seeders;
use App\Models\Item;
use App\Models\Sell;
use App\Models\SellSummary;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::insert([
            'name'=>'book',
            'price'=> 20.5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Item::insert([
            'name'=>'pen',
            'price'=> 5.0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Item::insert([
            'name'=>'phone',
            'price'=> 99.,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Sell::insert([
            'date'=> Carbon::now()->format('Y-m-d H:i:s'),
            'discount'=> 2,
            'price'=> 10,
            'item_id'=> 1,
            'employee_id'=> 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Sell::insert([
            'date'=> Carbon::now()->format('Y-m-d H:i:s'),
            'discount'=> 0,
            'price'=> 101,
            'item_id'=> 2,
            'employee_id'=> 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Sell::insert([
            'date'=> Carbon::now()->format('Y-m-d H:i:s'),
            'discount'=> 20,
            'price'=> 0.1,
            'item_id'=> 3,
            'employee_id'=> 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        SellSummary::insert([
            'date'=> Carbon::now()->format('Y-m-d H:i:s'),
            'price_total'=> 50,
            'discount_total'=> 20,
            'employee_id'=> 1,
            'total'=> 30,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        SellSummary::insert([
            'date'=> Carbon::now()->format('Y-m-d H:i:s'),
            'price_total'=> 100,
            'discount_total'=> 20,
            'employee_id'=> 3,
            'total'=> 80,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        SellSummary::insert([
            'date'=> Carbon::now()->format('Y-m-d H:i:s'),
            'price_total'=> 20,
            'discount_total'=> 20,
            'employee_id'=> 1,
            'total'=> 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
