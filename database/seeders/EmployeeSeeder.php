<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::insert([
            'companies_id' => '1',
            'first_name' => 'Leslie',
            'last_name' => 'Mervin',
            'email' => 'Leslie@gmail.com',
            'phone' => '12345',
            'password' => bcrypt('password'),
            'created_by_id'=> '1',
            'updated_by_id'=> '1'
        ]);

        Employee::insert([
            'companies_id' => '2',
            'first_name' => 'Kevin',
            'last_name' => '.',
            'email' => 'Kevin@gmail.com',
            'phone' => '12345',
            'password' => bcrypt('password'),
            'created_by_id'=> '1',
            'updated_by_id'=> '1'
        ]);
        Employee::insert([
            'companies_id' => '3',
            'first_name' => 'Vincent',
            'last_name' => 'yang',
            'email' => 'Vincent@gmail.com',
            'phone' => '12345',
            'password' => bcrypt('password'),
            'created_by_id'=> '1',
            'updated_by_id'=> '1'
        ]);
    }
}
