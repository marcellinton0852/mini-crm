<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Item;
use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Database\Seeder;
use Spatie\TranslationLoader\LanguageLine;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);

        $this->call([SpreadsheetSeeder::class,]);

        User::insert([
            'name' => 'user1',
            'email' => 'user@1.com',
            'password' => bcrypt('password')
        ]);
        $this->call([ItemSeeder::class]);
        $this->call([EmployeeSeeder::class]);

        // Testing

        LanguageLine::create([
        'group' => 'message',
        'key' => 'this_is_a_test',
        'text' => ['en' => 'This is a test to see the langue line work', 'id' => 'Ini adalah sebuah percobaan seeder'],
        ]);

        LanguageLine::create([
        'group' => 'message',
        'key' => 'it_work',
        'text' => ['en' => 'The Database to view work', 'id' => 'dari database ke view index berkerja dengan baik'],
        ]);

        LanguageLine::create([
        'group' => 'validation',
        'key' => 'testing',
        'text' => ['en' => 'test1', 'id' => 'test2'],
        ]);

        // Auth

        LanguageLine::create([
        'group' => 'auth',
        'key' => 'failed',
        'text' => ['en' => 'These credentials do not match our records.', 'id' => 'Email/password yang anda masukkan salah!.'],
        ]);

        LanguageLine::create([
        'group' => 'auth',
        'key' => 'password',
        'text' => ['en' => 'The provided password is incorrect.', 'id' => 'Kata sandi yang anda masukkan salah!.'],
        ]);

        LanguageLine::create([
        'group' => 'auth',
        'key' => 'throttle',
        'text' => ['en' => 'Too many login attempts. Please try again in :seconds seconds.', 'id' => 'Terlalu banyak request login!,silahkan mencoba :seconds detik lagi.'],
        ]);

        // flash

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'companyCreated',
        'text' => ['en' => 'Company Successfully Created!', 'id' => 'Perusahaan Berhasil ditambahkan!'],
        ]);

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'sellCreated',
        'text' => ['en' => 'Sell Successfully Created!', 'id' => 'Penjualan Berhasil ditambahkan!'],
        ]);

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'itemCreated',
        'text' => ['en' => 'Item Successfully Created!', 'id' => 'Barang Berhasil ditambahkan!'],
        ]);

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'companyUpdated',
        'text' => ['en' => 'Company Successfully Updated!', 'id' => 'Perusahaan Berhasil diperbarui!'],
        ]);

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'companyRemoved',
        'text' => ['en' => 'Company Successfully Removed!', 'id' => 'Perusahaan Berhasil dihapus!'],
        ]);
        LanguageLine::create([
        'group' => 'flash',
        'key' => 'itemRemoved',
        'text' => ['en' => 'Item Successfully Removed!', 'id' => 'Barang Berhasil dihapus!'],
        ]);
        LanguageLine::create([
        'group' => 'flash',
        'key' => 'sellRemoved',
        'text' => ['en' => 'Sell Successfully Removed!', 'id' => 'Penjualan Berhasil dihapus!'],
        ]);

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'employeeCreated',
        'text' => ['en' => 'Employee Successfully Created!', 'id' => 'Karyawan berhasil ditambahkan'],
        ]);

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'employeeUpdated',
        'text' => ['en' => 'Employee Successfully Updated!', 'id' => 'Karyawan berhasil diperbarui'],
        ]);

        LanguageLine::create([
        'group' => 'flash',
        'key' => 'employeeRemoved',
        'text' => ['en' => 'Employee Successfully Removed!', 'id' => 'Karyawan berhasil dihapus'],
        ]);

        //pagination

        LanguageLine::create([
        'group' => 'pagination',
        'key' => 'previous',
        'text' => ['en' => '&laquo; Previous', 'id' => '&laquo; Sebelumnya'],
        ]);
        LanguageLine::create([
        'group' => 'pagination',
        'key' => 'next',
        'text' => ['en' => 'Next &raquo;', 'id' => 'Selanjutnya &raquo;'],
        ]);

        //passwords

        LanguageLine::create([
        'group' => 'passwords',
        'key' => 'reset',
        'text' => ['en' => 'Your password has been reset!', 'id' => 'Kata sandi berhasil di reset!'],
        ]);

        LanguageLine::create([
        'group' => 'passwords',
        'key' => 'sent',
        'text' => ['en' => 'We have emailed your password reset link!', 'id' => 'Kami sudah mengirimkan email untuk link reset kata sandi anda!'],
        ]);

        LanguageLine::create([
        'group' => 'passwords',
        'key' => 'throttled',
        'text' => ['en' => 'Please wait before retrying.', 'id' => 'Tunggu sebentar sebelum mencoba lagi'],
        ]);

        LanguageLine::create([
        'group' => 'passwords',
        'key' => 'token',
        'text' => ['en' => 'This password reset token is invalid.', 'id' => 'Token reset kata sandi sudah invalid'],
        ]);

        LanguageLine::create([
        'group' => 'passwords',
        'key' => 'user',
        'text' => ['en' => "We can't find a user with that email address.", 'id' => 'Kami tidak bisa mendapatkan user yang menggunakan email tersebut.'],
        ]);

        //validation

        LanguageLine::create([
        'group' => 'validation',
        'key' => 'accepted',
        'text' => ['en' => 'The :attribute must be accepted.', 'id' => 'The :attribute must be accepted.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'accepted_if',
        'text' => ['en' => 'The :attribute must be accepted when :other is :value.', 'id' => 'The :attribute must be accepted when :other is :value.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'active_url',
        'text' => ['en' => 'The :attribute is not a valid URL.', 'id' => 'The :attribute is not a valid URL.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'after',
        'text' => ['en' => 'The :attribute must be a date after :date.', 'id' => 'The :attribute must be a date after :date.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'after_or_equal',
        'text' => ['en' => 'The :attribute must be a date after or equal to :date.', 'id' => 'The :attribute must be a date after or equal to :date.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'alpha',
        'text' => ['en' => 'The :attribute must only contain letters.', 'id' => 'The :attribute must only contain letters.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'alpha_dash',
        'text' => ['en' => 'The :attribute must only contain letters, numbers, dashes and underscores.', 'id' => 'The :attribute must only contain letters, numbers, dashes and underscores.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'alpha_num',
        'text' => ['en' => 'The :attribute must only contain letters and numbers.', 'id' => 'The :attribute must only contain letters and numbers.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'array',
        'text' => ['en' => 'The :attribute must be an array.', 'id' => 'The :attribute must be an array.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'before',
        'text' => ['en' => 'The :attribute must be a date before :date.', 'id' => 'The :attribute must be a date after or equal to :date.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'before_or_equal',
        'text' => ['en' => 'The :attribute must be a date before or equal to :date.', 'id' => 'The :attribute must be a date before or equal to :date.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'boolean',
        'text' => ['en' => 'The :attribute field must be true or false.', 'id' => 'The :attribute field must be true or false.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'confirmed',
        'text' => ['en' => 'The :attribute confirmation does not match.', 'id' => 'The :attribute confirmation does not match.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'current_password',
        'text' => ['en' => 'The password is incorrect.', 'id' => 'The password is incorrect.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'date',
        'text' => ['en' => 'The :attribute is not a valid date.', 'id' => 'The :attribute is not a valid date.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'date_equals',
        'text' => ['en' => 'The :attribute must be a date equal to :date.', 'id' => 'The :attribute must be a date equal to :date.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'date_format',
        'text' => ['en' => 'The :attribute does not match the format :format.', 'id' => 'The :attribute does not match the format :format.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'different',
        'text' => ['en' => 'The :attribute and :other must be different.', 'id' => 'The :attribute and :other must be different.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'digits',
        'text' => ['en' => 'The :attribute must be :digits digits.', 'id' => 'The :attribute must be :digits digits.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'digits_between',
        'text' => ['en' => 'The :attribute must be between :min and :max digits.', 'id' => 'The :attribute must be between :min and :max digits.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'dimensions',
        'text' => ['en' => 'The :attribute has invalid image dimensions.', 'id' => 'The :attribute has invalid image dimensions.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'distinct',
        'text' => ['en' => 'The :attribute field has a duplicate value.', 'id' => 'The :attribute field has a duplicate value.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'email',
        'text' => ['en' => 'The :attribute must be a valid email address.', 'id' => 'The :attribute must be a valid email address.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'ends_with',
        'text' => ['en' => 'The :attribute must end with one of the following: :values.', 'id' => 'The :attribute must end with one of the following: :values.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'exists',
        'text' => ['en' => 'The selected :attribute is invalid.', 'id' => 'The selected :attribute is invalid.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'file',
        'text' => ['en' => 'The :attribute must be a file.', 'id' => 'The :attribute must be a file.'],
        ]);
        LanguageLine::create([
        'group' => 'validation',
        'key' => 'filled',
        'text' => ['en' => 'The :attribute field must have a value.', 'id' => 'The :attribute field must have a value.'],
        ]);

        // .jon or message
        LanguageLine::create([
        'group' => 'message',
        'key' => 'home',
        'text' => ['en' => 'Home', 'id' => 'Beranda'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'logout',
        'text' => ['en' => 'Logout', 'id' => 'Keluar'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'add company',
        'text' => ['en' => 'Add Company', 'id' => 'Tambah'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'company',
        'text' => ['en' => 'Company', 'id' => 'Perusahan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'employee',
        'text' => ['en' => 'Employee', 'id' => 'Karyawan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'Dashboard',
        'text' => ['en' => 'Dashboard', 'id' => 'Beranda'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'company_name',
        'text' => ['en' => 'Company Name', 'id' => 'Nama'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'insert logo here',
        'text' => ['en' => 'Insert Logo here', 'id' => 'Masukkan Logo disini'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'action',
        'text' => ['en' => 'Action', 'id' => 'Aksi'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'Phone',
        'text' => ['en' => 'Phone', 'id' => 'Telepon'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'search',
        'text' => ['en' => 'Search', 'id' => 'Cari'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'View Company Employee',
        'text' => ['en' => 'View Company Employee', 'id' => 'Lihat Karyawan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'Phone number',
        'text' => ['en' => 'Phone number', 'id' => 'Nomor telepon'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'Back to Company',
        'text' => ['en' => 'Back to Company', 'id' => 'Kembali ke'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'filter',
        'text' => ['en' => 'Filter', 'id' => 'Filter'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'name',
        'text' => ['en' => 'Name', 'id' => 'Nama'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'website',
        'text' => ['en' => 'Website', 'id' => 'Website'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'email',
        'text' => ['en' => 'Email', 'id' => 'Email'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'selected',
        'text' => ['en' => 'Selected', 'id' => 'Terpilih'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'pages',
        'text' => ['en' => 'Pages', 'id' => 'Halaman'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'time_zone',
        'text' => ['en' => 'Time Zone', 'id' => 'Zona Waktu'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'search',
        'text' => ['en' => 'Search', 'id' => 'Mencari'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'type_here',
        'text' => ['en' => 'Type Here', 'id' => 'Ketik Disini'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'first_name',
        'text' => ['en' => 'First Name', 'id' => 'Nama Depan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'last_name',
        'text' => ['en' => 'Last Name', 'id' => 'Nama Belakang'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'phone',
        'text' => ['en' => 'Phone Number', 'id' => 'No.Hp'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'companies_id',
        'text' => ['en' => 'Companies ID', 'id' => 'ID Perusahaan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'item',
        'text' => ['en' => 'Item', 'id' => 'Barang'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'item_list',
        'text' => ['en' => 'Item List', 'id' => 'List Barang'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'sell',
        'text' => ['en' => 'Sell', 'id' => 'Penjualan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'sell_summary',
        'text' => ['en' => 'Sell Summary', 'id' => 'Laporan Penjualan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'add_item',
        'text' => ['en' => 'Add Item', 'id' => 'Menambahkan Barang'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'price',
        'text' => ['en' => 'Price', 'id' => 'Harga'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'item_name',
        'text' => ['en' => 'Item Name', 'id' => 'Nama Barang'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'discount',
        'text' => ['en' => 'Discount', 'id' => 'Diskon'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'employee_name',
        'text' => ['en' => 'Employee Name', 'id' => 'Nama Karyawan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'add_item_sold',
        'text' => ['en' => 'Add Item Sold', 'id' => 'Menambah Barang Kejual'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'item_sold_name',
        'text' => ['en' => 'Item Sold Name', 'id' => 'Nama Barang Terjual'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'item_discount',
        'text' => ['en' => 'Item Discount', 'id' => 'Diskon Barang'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'created_date',
        'text' => ['en' => 'Created Date', 'id' => 'Tanggal Pembuatan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'sell_date',
        'text' => ['en' => 'Sell Date', 'id' => 'Tanggal Penjualan'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'price_total',
        'text' => ['en' => 'Total Price', 'id' => 'Jumlah Harga'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'discount_total',
        'text' => ['en' => 'Total Discount', 'id' => 'Jumlah Diskon'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'total',
        'text' => ['en' => 'Total', 'id' => 'Jumlah'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'date',
        'text' => ['en' => 'Date', 'id' => 'Tanggal'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'from',
        'text' => ['en' => 'From', 'id' => 'Dari'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'to',
        'text' => ['en' => 'To', 'id' => 'Ke'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'from_date',
        'text' => ['en' => 'From Date', 'id' => 'Dari Tanggal'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => 'to_date',
        'text' => ['en' => 'To Date', 'id' => 'Ke Tanggal'],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => '',
        'text' => ['en' => '', 'id' => ''],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => '',
        'text' => ['en' => '', 'id' => ''],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => '',
        'text' => ['en' => '', 'id' => ''],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => '',
        'text' => ['en' => '', 'id' => ''],
        ]);
        LanguageLine::create([
        'group' => 'message',
        'key' => '',
        'text' => ['en' => '', 'id' => ''],
        ]);
    }
}
