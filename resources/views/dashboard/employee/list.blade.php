@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2" style="background-color: white">
        <div class="col-lg-12">
            @if($message = Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('success') !!}
                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <label>Filter</label>
                                    <form action="/employee/list" method="GET">
                                        <select id="filter" class="form-control filter" name="filter">
                                            <option value="">Default</option>
                                            <option value="first_name">First Name</option>
                                            <option value="last_name">Last Name</option>
                                            <option value="companies_id">Company</option>
                                            <option value="email">Email</option>
                                            <option value="phone">Phone</option>
                                        </select>
                                        <div class="form-group">
                                            <select name="paginate" id="paginate">
                                                <option value="10">10 Pages</option>
                                                <option value="15">15 Pages</option>
                                                <option value="20">20 Pages</option>
                                                <option value="25">25 Pages</option>
                                                <option value="50">50 Pages</option>
                                                <option value="">100 Pages</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <input name='string' type="text" class="form-control" placeholder="Type Here" aria-label="Type Here" aria-describedby="button-addon2">
                                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                                          </div>
                                          <select name="timezone" id="timezone" class="form-control timezone">
                                            <option value="">Time Zone(Default)</option>
                                            <option value="Asia/Jakarta">Jakarta</option>
                                            <option value="Asia/Singapore">Singapore</option>
                                            <option value="Asia/Tokyo">Tokyo</option>
                                        </select>
                                    </form>
            <table id="employee" class="display table table-bordered table-striped">
                <thead class="table">
                    <tr>
                        <th>No</th>
                        <th>{{ __('First Name') }}</th>
                        <th>{{ __('Last Name') }}</th>
                        <th>{{ __('Company Name') }}</th>
                        <th>Email</th>
                        <th>{{ __('Phone') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employees as $employee)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $employee->first_name }}</td>
                        <td>{{ $employee->last_name }}</td>
                        <td><a href="/dashboard/company/{{ $employee->companies->id }}">{{ $employee->companies->name }}</a></td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td>{{ carbon\Carbon::parse($employee->created_at)->setTimezone(request('timezone')) }}</td>
                        <td>
                            <form class="d-inline" action="{{ route('employee.destroy',$employee) }}" method="post">
                                <a href="{{ route('employee.show',$employee) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                <a href="{{ route('employee.edit',$employee) }}" class="btn btn-dark btn-sm"><i class="fa fa-pencil"></i></a>
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" onclick="return confirm('Confirm to delete data')" ><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection
