@extends('dashboard.layouts.main')

@section('container')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-uppercase font-weight-bold">{{ $item->name }}</h3>
            <div class="card-tools">
                {{-- <a href="{{ route('company.employee', $company) }}">{{ __('View Company Employee') }}</a> --}}
            </div>
        </div>
        <div class="card-body">
            <img class="card-img-top mr-4" src="{{ asset('/img/boxes.jpg') }}" style="width:150px; float: left;"></td>
            <ul class="mt-5">
                <li>{{__("name")}} : {{ $item->name }}</li>
                <li>{{__("price")}} : {{ $item->price }}</li>
            </ul>
        </div>
    </div>
@endsection
