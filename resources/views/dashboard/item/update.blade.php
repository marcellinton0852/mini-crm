@extends('dashboard.layouts.main')

@section('container')
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-7">
            <form action="{{ route('item.update', $item) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group mb-3">
                    <label for="name">{{ __('message.item_name') }}</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="{{__('item_name')}}" value="{{ $item->name }}">
                    @error('name')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="price">{{__('price')}}</label>
                    <input type="text" class="form-control" id="price" name="price" placeholder="{{__('price')}}" value="{{ $item->price }}">
                    @error('price')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
