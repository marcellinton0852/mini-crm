@extends('dashboard.layouts.main')

@section('container')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-7">
                <form action="{{ route('item.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="name">{{ __('message.item_name') }}</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('message.item_name') }}" autocomplete="off" value="{{ old('name') }}">
                        @error('name')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="price">{{__("message.price")}}</label>
                        <input type="text" class="form-control" id="price" name="price" placeholder="{{ __('message.price') }}" autocomplete="off" value="{{ old('price') }}">
                        @error('price')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <script>
    </script>
@endsection
