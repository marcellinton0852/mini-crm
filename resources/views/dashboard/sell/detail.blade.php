@extends('dashboard.layouts.main')

@section('container')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-uppercase font-weight-bold">{{ $sell->item->name }}</h3>
            <div class="card-tools">
                {{-- <a href="{{ route('company.employee', $company) }}">{{ __('View Company Employee') }}</a> --}}
            </div>
        </div>
        <div class="card-body">
            <img class="card-img-top mr-4" src="{{ asset('/img/boxes.jpg') }}" style="width:150px; float: left;"></td>
            <h1>{{__("message.name")}} : {{ $sell->item->name }}</h1>
            <ul class="mt-2">
                <li>{{__("message.sell_date")}} : {{ $sell->date }}</li>
                <li>{{__('message.price')}} : {{ $sell->price}}</li>
                <li>{{__('message.discount')}} : {{ $sell->discount}}</li>
                <li>{{__('message.employee')}} : {{ $sell->employee->first_name}} {{$sell->employee->last_name}}</li>
            </ul>
        </div>
    </div>
@endsection
