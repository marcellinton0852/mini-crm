@extends('dashboard.layouts.main')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Item Sold</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Item Sold</li>
                </ol>
              </div>
        </div>
    </div>
</div>
    <div class="content">
        <div class="container-fluid ">
            <div class="row card-header" style="background:white">
                <div class="col-lg-10 justify-content-center">
                    <div class="row pb-2">
                        <div class="col ml-2">
                            <a href="/dashboard/sell/create" class="text-white"><button type="button" class="btn btn-dark">{{ __('message.add_item_sold') }}</a></button>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="filter" class="form-label">{{__('message.filter')}}</label>
                                    <form action="/dashboard/sell" method="GET">
                                        <select id="filter" class="form-control filter" name="filter">
                                            @if (request('filter'))
                                                <option value="{{ request('filter') }}" selected>{{__('message.selected')}} ({{ ucfirst(request('filter'))}})</option>
                                            @else
                                                <option value="" selected>Default</option>
                                            @endif
                                                <option value="">Default</option>
                                                <option value="price">{{__('message.price')}}</option>
                                                <option value="company_name">{{__('message.company')}}</option>
                                                <option value="employee_name">{{__('message.employee_name')}}</option>
                                                <option value="discount">{{__('message.discount')}}</option>
                                        </select>
                                        <div>
                                            <div class="form">
                                                <label for="from">{{__('message.from')}}</label>
                                                <input type="date", id="from" name="from" class="form-control" value="{{date('Y-m-d')}}">&nbsp;
                                            </div>
                                            <div class="form">
                                                <label for="to">{{__('message.to')}}</label>
                                                <input type="date", id="to" name="to" class="form-control"value="{{date('Y-m-d')}}">&nbsp;
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <select name="paginate" id="paginate">
                                                @if (request('paginate'))
                                                    <option value="{{ request('paginate') }}" selected>{{ ucfirst(request('paginate'))}} {{__('message.pages')}}</option>
                                                @else
                                                    <option value="15" selected>15 {{__('message.pages')}}(Default)</option>
                                                @endif
                                                    <option value="10">10 {{__('message.pages')}}</option>
                                                    <option value="15">15 {{__('message.pages')}}(Default)</option>
                                                    <option value="20">20 {{__('message.pages')}}</option>
                                                    <option value="25">25 {{__('message.pages')}}</option>
                                                    <option value="50">50 {{__('message.pages')}}</option>
                                            </select>
                                    </div>
                                        <div class="input-group mb-3">
                                            <input name='string' type="text" class="form-control" placeholder="Type Here" aria-label="Type Here" aria-describedby="button-addon2" value="{{ request('string') }}">
                                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">{{__('message.search')}}</button>
                                          </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($message = Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! session('success') !!}
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <table id="item" class="display table table-bordered table-striped">
                        <thead class="table">
                            <tr>
                                <th>No</th>
                                <th>{{__("message.sell_date")}}</th>
                                <th>{{ __('message.item_name') }}</th>
                                <th>{{ __('message.price') }}</th>
                                <th>{{ __('message.discount') }}</th>
                                <th>{{ __('message.employee_name') }}</th>
                                <th>{{ __('message.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sells as $sell)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$sell->date}}</td>
                                <td>{{$sell->item->name}}</td>
                                <td>Rp. {{number_format($sell->price, 0, ',' , '.')}}</td>
                                <td>{{number_format($sell->discount, 0, ',','.')}}%</td>
                                <td>{{$sell->employee->first_name}} {{$sell->employee->last_name}}</td>
                                <td>
                                    <form class="d-inline" action="{{ route('sell.destroy',$sell) }}" method="post">
                                        <a href="{{ route('sell.show',$sell) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                        <a href="{{ route('sell.edit',$sell) }}" class="btn btn-dark btn-sm"><i class="fa fa-pencil"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="return confirm('Confirm to delete data')" ><i class="fas fa-trash-alt"></i></a>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$sells->links()}}
@endsection
