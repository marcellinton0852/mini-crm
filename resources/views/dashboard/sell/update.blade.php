@extends('dashboard.layouts.main')

@section('container')
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-7">
            <form action="{{ route('sell.update', $sell) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="item_name" class="form-label">{{__('message.item_name')}}</label>
                        <select id="item_name" class="form-control item_name" name="item_id">
                            @foreach ($items as $item)
                                @if ($sell->item->id == $item->id)
                                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                @else
                                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                    <label for="employee" class="form-label">{{__('message.employee')}}</label>
                        <select id="employee" class="form-control employee" name="employee_id">
                            @foreach ($employees as $employee)
                                @if ($sell->employee->id == $employee->id)
                                    <option value="{{$employee->id}}" selected>{{$employee->first_name}} {{$employee->last_name}}</option>
                                @else
                                    <option value="{{$employee->id}}">{{$employee->first_name}} {{$employee->last_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="date">{{__('message.sell_date')}} </label>
                        <input type="date", id="date" name="date" class="form-control" value="{{ old('date', date('Y-m-d')) }}">
                        @error('discount')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="discount">{{ __('message.discount') }} ({{$sell->discount}})</label>
                        <input type="text" class="form-control" id="discount" name="discount" placeholder="{{ __('message.item_discount') }}" autocomplete="off" value="0">
                        @error('discount')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="price">{{__('message.price')}} ({{$sell->price}})</label>
                        <input type="text" class="form-control" id="price" name="price" placeholder="{{ __('message.price') }}" autocomplete="off" value="0">
                        @error('price')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
