<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link center">
      CRM
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/img/profile.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/dashboard" class="d-block">{{ str_contains(auth()->guard()->getName(), 'web') ? auth()->user()->name : auth()->user()->first_name .' '. auth()->user()->last_name }}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="{{ __('Search') }}" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               @if(str_contains(auth()->guard()->getName(), 'web'))
              <li class="nav-item">
                <a href="/dashboard" class="nav-link {{ (request()->is('dashboard')) ? 'active' : '' }}">
                  <i class="fas fa-home nav-icon"></i>
                  <p>{{ __('Dashboard') }}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/dashboard/company" class="nav-link {{ (request()->is('dashboard/company*')) ? 'active' : '' }}">
                  <i class="far fa-building nav-icon"></i>
                  <p>{{ __('message.company') }}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/dashboard/employee" class="nav-link {{ (request()->is('dashboard/employee*')) ? 'active' : '' }} ">
                  <i class="fas fa-user-tie nav-icon"></i>
                  <p>{{ __('message.employee') }}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link {{ (request()->is('dashboard/item', 'dashboard/sell', 'dashboard/sellsummary')) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-book"></i>
                  <p>
                    {{ __('message.item') }}
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/dashboard/item" class="nav-link {{ (request()->is('dashboard/item')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>{{__('message.item_list')}}</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/dashboard/sell" class="nav-link {{ (request()->is('dashboard/sell')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>{{__('message.sell')}}</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/dashboard/sellsummary" class="nav-link {{ (request()->is('dashboard/sellsummary')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>{{__('message.sell_summary')}}</p>
                    </a>
                  </li>
                </ul>
              </li>
              @else
            <li class="nav-item">
            <a href="/employee/dashboard" class="nav-link {{ (request()->is('/employee/dashboard')) ? 'active' : '' }}">
              <i class="nav-icon far  fa-building"></i>
              <p>
                {{ __('Company Sites') }}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/employee/list" class="nav-link {{ (request()->is('/employee/list*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                {{ __('Employees Users') }}
                <span class="right badge badge-danger">{{ __('New') }}</span>
              </p>
            </a>
          </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
