@extends('dashboard.layouts.main')

@section('container')
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-7">
            <form action="{{ route('company.update', $company) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group mb-3">
                    <label for="name">{{ __('Company Name') }}</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Company name" value="{{ $company->name }}">
                    @error('name')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="example@example.com" value="{{ $company->email }}">
                    @error('email')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="website">Website</label>
                    <input type="text" class="form-control" id="website" name="website" placeholder="www.example.com" value="{{ $company->website }}">
                    @error('website')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label class="d-block" for="logo" class="formFile">{{ __('Insert Logo here') }}</label>
                    <img src="{{ asset('storage/' . $company->logo) }}" class="img-preview mb-3 d-block" style="width: 100px;">
                    <input type="file" id="logo" name="logo" onchange="imgPreview()">
                    @error('logo')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
</div>

<script>
    function imgPreview() {
        const image = document.querySelector('#logo')
        const preview = document.querySelector('.img-preview')

        preview.style.display = 'block'

        const oFReader = new FileReader();
        oFReader.readAsDataURL(image.files[0])

        oFReader.onload = function(oFREvent) {
            preview.src = oFREvent.target.result;
        }
    }
</script>
@endsection