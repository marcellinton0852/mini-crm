@extends('dashboard.layouts.main')

@section('container')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-uppercase font-weight-bold">{{ $company->name }}</h3>
            <div class="card-tools">
                <a href="{{ route('company.employee', $company) }}">{{ __('View Company Employee') }}</a>
            </div>
        </div>
        <div class="card-body">
            @if($company->logo == 'default.png')
                <img class="card-img-top mr-4" src="{{ asset('img/default.png') }}" style="width: 150px; float:left;">
            @else
                <img class="card-img-top mr-4" src="{{ asset('storage/' . $company->logo) }}" style="width:150px; float: left;"></td>
            @endif
            <ul class="mt-5">
                <li>Email : {{ $company->email }}</li>
                <li>Website : {{ $company->website }}</li>
            </ul>
        </div>
    </div>
@endsection