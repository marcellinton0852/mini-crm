@extends('dashboard.layouts.main')

@section('container')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-7">
                <form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="companies_id" value="{{ $company_id }}">
                    <div class="form-group mb-3">
                        <label for="first_name">{{ __('First Name') }}</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" autocomplete="off" value="{{ old('first_name') }}">
                        @error('first_name')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="last_name">{{ __('Last Name') }}</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" autocomplete="off" value="{{ old('last_name') }}">
                        @error('last_name')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="example@example.com" autocomplete="off" value="{{ old('email') }}">
                        @error('email')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <label for="phone">{{ __('Phone number') }}</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="12345678" autocomplete="off" value="{{ old('phone') }}">
                        @error('phone')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <label for="password">{{ __('Password') }}</label>
                        <input type="text" class="form-control" id="password" name="password" placeholder="qwerty" autocomplete="off" value="{{ old('password') }}">
                        @error('password')
                            <div class="text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
