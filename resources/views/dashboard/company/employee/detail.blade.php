@extends('dashboard.layouts.main')

@section('container')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-uppercase font-weight-bold">{{ $employee->first_name }}  {{ $employee->last_name }}</h3>
            <div class="card-tools">
                <span class="badge badge-primary">Option</span>
            </div>
        </div>
        <div class="card-body">
            <img src="/img/user.png" class="card-img-top mr-4" style="width:150px; float:left;" alt="...">
            <ul class="mt-5">
                <li>
                    {{ __('Company Name') }} : <a href="/dashboard/company/{{ $employee->companies->id }}" class="text-uppercase">{{ $employee->companies->name }}</a>
                </li>
                <li>Email : {{ $employee->email }}</li>
                <li>Phone : {{ $employee->phone }}</li>
            </ul>
        </div>
    </div>
@endsection
