@extends('dashboard.layouts.main')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Employee</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Employee</li>
                </ol>
              </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="container ml-2" style="background:white">
        <div class="col-lg-12">
            @if($message = Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('success') !!}
                    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <label for="filter" class="form-label">{{__('message.filter')}}</label>
            <form action="/dashboard/employee" method="GET">
                <select id="filter" class="form-control filter" name="filter">
                    @if (request('filter'))
                        <option value="{{ request('filter') }}" selected>{{__('message.selected')}} ({{ ucfirst(request('filter'))}})</option>
                    @else
                        <option value="" selected>Default</option>
                    @endif
                        <option value="">Default</option>
                        <option value="first_name">{{__('message.first_name')}}</option>
                        <option value="last_name">{{__('message.last_name')}}</option>
                        <option value="companies_id">{{__('message.companies_id')}}</option>
                        <option value="email">{{__('message.email')}}</option>
                        <option value="phone">{{__('message.phone')}}</option>
                </select>
                <div class="form-group">
                    <select name="paginate" id="paginate">
                        @if (request('paginate'))
                            <option value="{{ request('paginate') }}" selected>{{ ucfirst(request('paginate'))}} {{__('message.pages')}}</option>
                        @else
                            <option value="15" selected>15 {{__('message.pages')}}(Default)</option>
                        @endif
                            <option value="10">10 {{__('message.pages')}}</option>
                            <option value="15">15 {{__('message.pages')}}(Default)</option>
                            <option value="20">20 {{__('message.pages')}}</option>
                            <option value="25">25 {{__('message.pages')}}</option>
                            <option value="50">50 {{__('message.pages')}}</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <input name='string' type="text" class="form-control" placeholder="Type Here" aria-label="Type Here" aria-describedby="button-addon2">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">{{__('message.search')}}</button>
                  </div>
                  <select name="timezone" id="timezone" class="form-control timezone">
                    @if (request('timezone'))
                        <option value="{{ request('timezone') }}" selected>{{ ucfirst(request('timezone'))}}</option>
                    @else
                        <option value="" selected>{{__('message.time_zone')}}(Default)</option>
                    @endif
                        <option value="">Default</option>
                        <option value="Asia/Jakarta">Jakarta</option>
                        <option value="Asia/Singapore">Singapore</option>
                        <option value="Asia/Tokyo">Tokyo</option>
                </select>
            </form>
            <table id="employee" class="display table table-bordered table-striped mt-2">
                <thead class="table">
                    <tr>
                        <th>No</th>
                        <th>{{ __('First Name') }}</th>
                        <th>{{ __('Last Name') }}</th>
                        <th>{{ __('Company Name') }}</th>
                        <th>Email</th>
                        <th>{{ __('Phone') }}</th>
                        <th>{{ __('Created At') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employees as $employee)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $employee->first_name }}</td>
                        <td>{{ $employee->last_name }}</td>
                        <td><a href="/dashboard/company/{{ $employee->companies->id }}">{{ $employee->companies->name }}</a></td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td>{{ carbon\Carbon::parse($employee->created_at)->setTimezone(request('timezone')) }}</td>
                        <td>
                            <form class="d-inline" action="{{ route('employee.destroy',$employee) }}" method="post">
                                <a href="{{ route('employee.show',$employee) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                <a href="{{ route('employee.edit',$employee) }}" class="btn btn-dark btn-sm"><i class="fa fa-pencil"></i></a>
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" onclick="return confirm('Confirm to delete data')" ><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
<script>
</script>

{{ $employees->links() }}
@endsection
