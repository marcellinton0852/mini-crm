@extends('dashboard.layouts.main')

@section('container')
{{-- title --}}
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Companies</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Companies</li>
                </ol>
              </div>
        </div>
    </div>
</div>
    <div class="content">
        <div class="container-fluid ">
            <div class="row card-header" style="background:white">
                <div class="col-lg-10 justify-content-center">
                    <div class="row pb-2">
                        <div class="col ml-2">
                            <a href="/dashboard/company/create" class="text-white"><button type="button" class="btn btn-dark">{{ __('Add Company') }}</a></button>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="filter" class="form-label">{{__('message.filter')}}</label>
                                    <form action="/dashboard/company" method="GET">
                                        <select id="filter" class="form-control filter" name="filter">
                                            @if (request('filter'))
                                                <option value="{{ request('filter') }}" selected>{{__('message.selected')}} ({{ ucfirst(request('filter'))}})</option>
                                            @else
                                                <option value="" selected>Default</option>
                                            @endif
                                                <option value="">Default</option>
                                                <option value="name">{{__('message.name')}}</option>
                                                <option value="email">{{__('message.email')}}</option>
                                                <option value="website">{{__('message.website')}}</option>
                                        </select>
                                        <div class="form-group">
                                            <select name="paginate" id="paginate">
                                                @if (request('paginate'))
                                                    <option value="{{ request('paginate') }}" selected>{{ ucfirst(request('paginate'))}} {{__('message.pages')}}</option>
                                                @else
                                                    <option value="15" selected>15 {{__('message.pages')}}(Default)</option>
                                                @endif
                                                    <option value="10">10 {{__('message.pages')}}</option>
                                                    <option value="15">15 {{__('message.pages')}}(Default)</option>
                                                    <option value="20">20 {{__('message.pages')}}</option>
                                                    <option value="25">25 {{__('message.pages')}}</option>
                                                    <option value="50">50 {{__('message.pages')}}</option>
                                            </select>
                                        </div>
                                        <div class="input-group mb-3">
                                            <input name='string' type="text" class="form-control" placeholder="Type Here" aria-label="Type Here" aria-describedby="button-addon2">
                                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">{{__('message.search')}}</button>
                                          </div>
                                          <select name="timezone" id="timezone" class="form-control timezone">
                                            @if (request('timezone'))
                                                <option value="{{ request('timezone') }}" selected>{{ ucfirst(request('timezone'))}}</option>
                                            @else
                                                <option value="UTC" selected>{{__('message.time_zone')}}(Default)</option>
                                            @endif
                                                <option value="">Default</option>
                                                <option value="Asia/Jakarta">Jakarta</option>
                                                <option value="Asia/Singapore">Singapore</option>
                                                <option value="Asia/Tokyo">Tokyo</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($message = Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! session('success') !!}
                            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <table id="company" class="display table table-bordered table-striped">
                        <thead class="table">
                            <tr>
                                <th>No</th>
                                <th>{{ __('Company Name') }}</th>
                                <th>Email</th>
                                <th>Logo</th>
                                <th>Website</th>
                                <th>Create at</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $company)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $company->name }}</td>
                                <td>{{ $company->email }}</td>
                                <td>
                                    @if($company->logo == 'default.png')
                                    <img src="{{ asset('img/default.png') }}" style="width: 35px;">
                                    @else
                                    <img src="{{ asset('storage/'. $company->logo) }}" style="width:35px"></td>
                                    @endif
                                <td>{{ $company->website }}</td>
                                <td>{{ carbon\Carbon::parse($company->created_at)->setTimezone(request('timezone')) }}</td>
                                <td>
                                    <form class="d-inline" action="{{ route('company.destroy',$company) }}" method="post">
                                        <a href="{{ route('employee.create',$company) }}" class="btn btn-primary btn-sm" alt="Create Company"><i class="fas fa-user-plus"></i></a>
                                        <a href="{{ route('company.show',$company) }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                        <a href="{{ route('company.edit',$company) }}" class="btn btn-dark btn-sm"><i class="fa fa-pencil"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="return confirm('Confirm to delete data')" ><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
     </script>
     {{ $companies->links() }}
@endsection

