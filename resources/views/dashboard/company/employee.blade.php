@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2 mt-3">
        <div class="col-lg-12">
            <div class="mb-3">
            <a href="{{ route('company.index') }}"><button class="btn btn-dark">{{ __('Back to Company') }}</button></a>
            </div>
            <table id="employee" class="display table table-bordered table-striped">
                <thead class="table">
                    <tr>
                        <th>No</th>
                        <th>{{ __('First Name') }}</th>
                        <th>{{ __('Last Name') }}</th>
                        <th>Email</th>
                        <th>{{ __('Phone') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employees as $employee)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $employee->first_name }}</td>
                        <td>{{ $employee->last_name }}</td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td>
                            <form class="d-inline" action="{{ route('employee.destroy',$employee) }}" method="post">
                                <a href="{{ route('employee.show',$employee) }}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                                <a href="{{ route('employee.edit',$employee) }}" class="btn btn-dark btn-sm"><i class="fa fa-pencil"></i></a>
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" onclick="return confirm('Confirm to delete data')" ><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#employee').DataTable( {
            "autoWidth": false
        } );
    } );
</script>
@endsection
