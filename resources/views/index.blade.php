@extends('layouts.main')

@section('container')
<main class="form-signin">
  <form action="/login" method="post">
    @csrf
    <h1 class="h3 mb-3 fw-normal text-center">Please login</h1>
    @if(Session::has('failed'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('failed') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif
    <div class="form-floating">
      <input type="email" name="email" class="form-control" id="floatingInput" placeholder="name@example.com" autocomplete="off">
      <label for="floatingInput">Email address</label>
      @error('email')
          <div class="text-danger mb-2 mt-2">
              {{ $message }}
          </div>
      @enderror
    </div>
    <div class="form-floating">
      <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
      <label for="floatingPassword">Password</label>
      @error('password')
          <div class="text-danger mb-2">
              {{ $message }}
          </div>
      @enderror
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2021–2021</p>
  </form>
</main>
@endsection
