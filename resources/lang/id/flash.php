<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Flash Message
    |--------------------------------------------------------------------------
    |
    | 
    | 
    | 
    |
    */

    'companyCreated' => 'Perusahaan Berhasil ditambahkan!',
    'companyUpdated' => 'Perusahaan Berhasil diperbarui!',
    'companyRemoved' => 'Perusahaan Berhasil dihapus!',
    'employeeCreated' => 'Karyawan berhasil ditambahkan',
    'employeeUpdated' => 'Karyawan berhasil diperbarui',
    'employeeRemoved' => 'Karyawan berhasil dihapus',

];