<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email/password yang anda masukkan salah!.',
    'password' => 'Kata sandi yang anda masukkan salah!.',
    'throttle' => 'Terlalu banyak request login!,silahkan mencoba :seconds detik lagi.',

];
