<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Kata sandi berhasil di reset!',
    'sent' => 'Kami sudah mengirimkan email untuk link reset kata sandi anda!',
    'throttled' => 'Tunggu sebentar sebelum mencoba lagi',
    'token' => 'Token reset kata sandi sudah invalid',
    'user' => 'Kami tidak bisa mendapatkan user yang menggunakan email tersebut.',

];
