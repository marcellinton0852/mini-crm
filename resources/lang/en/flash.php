<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Flash Message
    |--------------------------------------------------------------------------
    |
    | 
    | 
    | 
    |
    */

    'companyCreated' => 'Company Successfully Created!',
    'companyUpdated' => 'Company Successfully Updated!',
    'companyRemoved' => 'Company Successfully Removed!',
    'employeeCreated' => 'Employee Successfully Created!',
    'employeeUpdated' => 'Employee Successfully Updated!',
    'employeeRemoved' => 'Employee Successfully Removed!',

];