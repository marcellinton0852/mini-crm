<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellSummary extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function sell()
    {
        return $this->belongsTo(Sell::class);
    }

    static function filter($filter,$data,$page,)
    {
        return self::where("$filter",'like', "%$data%")->latest()->paginate($page)->withQueryString();
    }
}
