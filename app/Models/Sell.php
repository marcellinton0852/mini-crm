<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function summary()
    {
        return $this->hasMany(Sell::class);
    }

    static function filter($filter,$data,$page)
    {
        return self::where("$filter", 'like', "%$data%")->latest()->paginate($page)->withQueryString();
    }

}
