<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    use HasFactory;


    protected $guarded = ['id'];
    protected $with = ['companies'];

    protected $hidden = [
        'password'
       ];

    public function companies()
    {
        return $this->belongsTo(Companies::class);
    }

    public function sells()
    {
        return $this->hasMany(Sell::class);
    }

    public function sellsummary()
    {
        return $this->hasMany(SellSummary::class);
    }

    public function index()
    {
        return view('dashboard.company.users.index');
    }


}
