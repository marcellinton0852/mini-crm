<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Companies;
use Illuminate\Http\Request;
use Faker\Provider\ar_JO\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Notifications\CompanyCreated;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->paginate;

        if($request->filter){
            if($request->filter == 'name') {
                $companies = Companies::where('name', 'like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'email'){
                $companies = Companies::where('email','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'website'){
                $companies = Companies::where('website','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
        }
        else {
            $companies = Companies::latest()->paginate($request->get('paginate'));

        };

        return view('dashboard.company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user(); // Retrieve the currently authenticated user...
        $id = Auth::id([]); // Retrieve the currently authenticated user's ID...

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'website' => 'required',
            'logo' => ' image|dimensions:min_width=100,min_height=100',
        ]);


        $input = $request->all();

        if ($request->file('logo')) {
            $input['logo'] = $request->file('logo')->store('logo');
        } else {
            $input['logo'] = "default.png";
        }
        $input['created_by_id'] = $id;
        $input['updated_by_id'] = $id;

        $test = Companies::create($input);
        $user = User::first();

        $createdData = [
            'header' => 'New Company has been created!',
            'body' => 'New company named ' . $request->get('name') .  ' has been created!',
            'text' => 'Go to Company',
            'url' => url('/dashboard/company'),
            'thankyou' => 'Thank you'
        ];

        $user->notify(new CompanyCreated($createdData));

        return redirect()->route('company.index')->with('success', __('flash.companyCreated'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function show(Companies $company)
    {
        return view('dashboard.company.detail', compact('company'));
    }

    public function showEmployee(Companies $company)
    {
        return view('dashboard.company.employee')->with('employees', $company->employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function edit(Companies $company)
    {

        return view('dashboard.company.update', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Companies $company)
    {
        $user = Auth::user(); // Retrieve the currently authenticated user...
        $id = Auth::id([]); // Retrieve the currently authenticated user's ID...

        $request->validate([
            'name' => 'required',
            'logo' => 'image|dimensions:min_width=100,min_height=100',
        ]);

        $input = $request->all();

        if ($request->file('logo')) {
            if($company['logo'] != 'default.png') Storage::delete($company['logo']);
            $input['logo'] = $request->file('logo')->store('logo');
        } else {
            unset($input['logo']);
        }
        $input['updated_by_id'] = $id;

        $company->update($input);

        return redirect()->route('company.index')->with('success', __('flash.companyUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companies $company)
    {
        $company->employee()->delete();
        if($company['logo'] != 'default.png') Storage::delete($company['logo']);
        $company->delete();

        return redirect()->route('company.index')->with('success', __('flash.companyRemoved'));
    }

}
