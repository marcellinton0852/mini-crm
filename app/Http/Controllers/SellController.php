<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Employee;
use App\Models\Companies;
use App\Models\Sell;
use App\Models\SellSummary;
use Illuminate\Http\Request;

class SellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $page = $request->paginate;
        $data = $request->string;

        if($filter = $request->filter) {
            if($filter == 'price' || $filter == 'discount') {
                $sells = Sell::filter($filter,$data,$page);
            } elseif($filter == 'employee_name') {
                $sells = Employee::where('first_name', 'like', "%$data%")->latest()->paginate($page)->withQueryString();
                $id = $sells->pluck('id');
                $sells = Sell::whereIn('employee_id', $id)->latest()->paginate($page)->withQueryString();
            } elseif($filter == 'company_name') {
                $sells = Companies::where('name', 'like', "%$data%")->latest()->paginate($page)->withQueryString();
                $id = $sells->pluck('id')[0];
                $sells = Employee::where('companies_id', 'like', "$id")->latest()->paginate($page)->withQueryString();
                $id = $sells->pluck('id');
                $sells = Sell::whereIn('employee_id', $id)->latest()->paginate($page)->withQueryString();
            }
            else {
                abort(404);
            }
        } elseif($request->from) {
            $sells = Sell::whereBetween('date', [$request->get('from'), $request->get('to')])->latest()->paginate($page)->withQueryString();
        } else {
            $sells = Sell::latest()->paginate($request->get('paginate'));
        }
        return view('dashboard.sell.index', compact('sells'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.sell.create',[
            'items' => Item::all(),
            'employees' => Employee::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sell $sell)
    {
        $request->validate([
            'date'=> 'required',
            'discount'=> 'required',
            'price'=> 'required',
            'item_id'=> 'required',
            'employee_id'=> 'required',
        ]);
        Sell::create($request->all());
        $employee_id = $request->employee_id;

        $date = date('Y-m-d',strtotime($request->date));

        if($summary = SellSummary::where([['employee_id', $employee_id], ['date', $date]])->first()){
            $summary->update([
                'price_total' => $summary->price_total + $request->price,
                'discount_total' => $summary->discount_total + $request->discount * $request->price/ 100,
                'total' => $summary->total + $request->price - $request->discount * $request->price /100,
                'updated_at' => date('Y-m-d')
            ]);

        } else {
            SellSummary::create([
                'date' => $date,
                'employee_id' => $request->employee_id,
                'price_total' => $request->price,
                'discount_total' => $request->discount * $request->price/ 100,
                'total' => $request->price - $request->discount * $request->price /100
            ]);
        }
        return redirect()->route('sell.index')->with('success',__('flash.sellCreated'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sell $sell)
    {
        return view('dashboard.sell.detail', compact('sell'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sell $sell)
    {
        return view('dashboard.sell.update', compact('sell'),[
            'items'=>Item::all(),
            'employees'=>Employee::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sell $sell, SellSummary $summary)
    {
        $employee_id = Sell::all()->first();

        $input = $request->validate([
            'date'=> 'required',
            'discount'=> 'required',
            'price'=> 'required',
            'item_id'=> 'required',
            'employee_id'=> 'required',
        ]);
        $input['price'] = $sell->price + $request->price;
        $input['discount'] = $sell->discount + $request->discount;

        $sell->update($input);
        $employee_id = $request->employee_id;

        $date = date('Y-m-d',strtotime($request->date));

        if($summary = SellSummary::where([['employee_id', $employee_id], ['date', $date]])->first()){
            $summary->update([
                'price_total' => $summary->price_total + $request->price,
                'discount_total' => $summary->discount_total + $request->discount * $request->price/ 100,
                'total' => $summary->total + $request->price - $request->discount * $request->price /100,
                'updated_at' => date('Y-m-d')
            ]);

        } else {
            SellSummary::create([
                'date' => $date,
                'employee_id' => $request->employee_id,
                'price_total' => $request->price,
                'discount_total' => $request->discount * $request->price/ 100,
                'total' => $request->price - $request->discount * $request->price /100
            ]);
        }

        return redirect()->route('sell.index', compact('sell'))->with('success', __('flash.sellCreated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sell $sell)
    {
        $sell->delete();
        return redirect()->route('sell.index')->with('success', __('flash.sellRemoved'));
    }
}
