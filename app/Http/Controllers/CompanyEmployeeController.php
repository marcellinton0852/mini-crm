<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyEmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->paginate;

        if($request->filter){
            if($request->filter == 'first_name') {
                $employees = Employee::where('first_name', 'like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'last_name'){
                $employees = Employee::where('last_name','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'companies_id'){
                $employees = Employee::where('companies_id','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'email'){
                $employees = Employee::where('email','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'phone'){
                $employees = Employee::where('phone','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
        }
        else {
            $employees = Employee::with('companies')->latest()->paginate($request->get('paginate'));
        }
        return view('dashboard.company.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->all();

        return view('dashboard.company.employee.create', [
            'company_id' => key($id)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user(); // Retrieve the currently authenticated user...
        $id = Auth::id([]); // Retrieve the currently authenticated user's ID...

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:dns',
            'phone' => 'required|numeric',
            'password' => 'required'
        ]);
        $employee = new Employee([
            "first_name" => $request->get('first_name'),
            "last_name" => $request->get('last_name'),
            "email" => $request->get('email'),
            'phone' => $request->get('phone'),
            'companies_id' => $request->get('companies_id'),
            'password' => bcrypt($request->get('password')),
            'created_by_id' => $id,
            'updated_by_id' => $id

        ]);
        $employee->save();

        return redirect()->route('company.index')->with('success', __('flash.employeeCreated'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('dashboard.company.employee.detail', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('dashboard.company.employee.update', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $user = Auth::user(); // Retrieve the currently authenticated user...
        $id = Auth::id([]); // Retrieve the currently authenticated user's ID...

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        $input = $request->only('first_name','last_name','email','phone','password');
        $input['updated_by_id'] = $id;

        $employee->update($input);

        return redirect()->route('employee.index')->with('success', __('flash.employeeUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employee.index')->with('success', __('flash.employeeRemoved'));
    }
}
