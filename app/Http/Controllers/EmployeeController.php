<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }
    public function index()
    {
        return view('dashboard.employee.index');
    }
    public function show(Request $request)
    {
        $page = $request->paginate;
        $company_id = auth()->user()->companies_id;

        if($request->filter){
            if($request->filter == 'first_name') {
                $employees = Employee::where(['company_id', $company_id],'first_name', 'like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'last_name'){
                $employees = Employee::where(['company_id', $company_id],'last_name','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'companies_id'){
                $employees = Employee::where(['company_id', $company_id],'companies_id','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'email'){
                $employees = Employee::where(['company_id', $company_id],'email','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
            elseif($request->filter == 'phone'){
                $employees = Employee::where(['company_id', $company_id],'phone','like', "%$request->string%")->latest()->paginate($page)->withQueryString();
            }
        }
        else {
            $employees = Employee::where('companies_id', $company_id)->paginate($request->get('paginate'));
        }
        return view('dashboard.employee.list', compact('employees'));
    }
}
