<?php

namespace App\Http\Controllers;

use App\Models\Sell;
use App\Models\Companies;
use App\Models\Employee;
use App\Models\SellSummary;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\See;

class SellSummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->paginate;
        $data = $request->string;

        if($filter= $request->filter) {
            if($filter == 'date') {
                $summary  = SellSummary::filter($filter,$data,$page);
            } elseif($filter == 'employee_name') {
                $summary = Employee::where('first_name', 'like', "%$data%")->latest()->paginate($page)->withQueryString();
                $id = $summary->pluck('id');
                $summary = SellSummary::whereIn('employee_id', $id)->latest()->paginate($page)->withQueryString();
            } elseif($filter == 'company_name') {
                $summary = Companies::where('name', 'like', "%$data%")->latest()->paginate($page)->withQueryString();
                $id = $summary->pluck('id')[0];
                $summary = Employee::where('companies_id', 'like', "$id")->latest()->paginate($page)->withQueryString();
                $id = $summary->pluck('id');
            $summary = SellSummary::whereIn('employee_id', $id)->latest()->paginate($page)->withQueryString();
            }
            else {
                abort(404);
            }
        }
        elseif($request->from) {
            $sells = Sell::whereBetween('date', [$request->get('from'), $request->get('to')])->latest()->paginate($page)->withQueryString();

        }
        else {
            $summary = SellSummary::latest()->paginate($request->get('paginate'));
        }
        return view('dashboard.sell.sellsummary.index', compact('summary'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sell $sell)
    {
        $sell->summary->update($request->validate());

        return redirect()->route('sell.index', compact('sell'))->with('success', __('flash.sellCreated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
