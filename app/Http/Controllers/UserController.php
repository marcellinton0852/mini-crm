<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\hash;

class UserController extends Controller
{

    public function login(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        $user = User::where('email', $fields['email'])->first();

        if(!Auth::attempt($fields)){
            return response([
                'message' => 'no'
            ], 401);
        }
        $token['token'] = $user->createToken('myToken')->plainTextToken;

        $respond = [
            'user' => $user,
            'token' => $token
        ];
        return $respond;
    }
    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'logout'
        ];
    }
}
