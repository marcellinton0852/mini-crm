<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Models\Companies;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class EmployeeLoginController extends Controller
{
    protected function guard()
    {
        return Auth::guard('employee');
    }

    public function login($id)
    {
        $company = Companies::where('id', $id)->first();
    if(!$company) abort(404);

        return view('auth.employeelogin', compact('company'));
    }

    public function dologin($id, Request $request)
    {
        $creds = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $employee = Employee::where([
            'companies_id' => $id,
            'email' => $request->email,
            ])->first();

        if(!$employee) return back()->withErrors(['email' => __('failed')]);

        if(Auth::guard('employee')->attempt($creds)){
            $request->session()->regenerate();

            return redirect()->route('employee.dashboard');
        }
        else{
            return back()->withErrors(['email'=> __('fail')]);
        }
    }

    public function logout(Request $request)
    {
        $id = auth()->user()->companies_id;

        Auth::guard('employee')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login.employee_view', $id);
    }


}
