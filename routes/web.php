<?php

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\CompanyEmployeeController;
use App\Http\Controllers\EmployeeLoginController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\SellController;
use App\Http\Controllers\SellSummaryController;
use App\Models\Employee;
use Carbon\CarbonTimeZone;
use Symfony\Component\Console\Input\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', '/login');
Auth::routes(['register' => false]);

Route::get('lang/{lang}', [LocalizationController::class, 'lang'])->name('lang.switch');
// Route::get('timezone/{timezone}', [LocalizationController::class, 'timezone'])->name('timezone.switch');

Route::get('/dashboard',[DashboardController::class,'index']);
Route::get('dashboard/company/{company}/employee', [CompaniesController::class, 'showEmployee'])->name('company.employee');
Route::resource('/dashboard/employee', CompanyEmployeeController::class);
Route::resource('/dashboard/company', CompaniesController::class);

// Employee
Route::prefix('employee')->group(function(){
    Route::get('login/{company}', [EmployeeLoginController::class, 'login'])->name('login.employee_view');
    Route::post('login/{company}', [EmployeeLoginController::class, 'dologin'])->name('login.employee');
    Route::post('logout', [EmployeeLoginController::class, 'dologin'])->name('employee.logout')->middleware('auth:employee');
    Route::get('dashboard', [EmployeeController::class, 'index'])->name('employee.dashboard');
    Route::get('list', [EmployeeController::class, 'show']);
});

// Item
Route::resource('/dashboard/item', ItemController::class);

// Sell
Route::resource('/dashboard/sell', SellController::class);

Route::resource('/dashboard/sellsummary', SellSummaryController::class);



